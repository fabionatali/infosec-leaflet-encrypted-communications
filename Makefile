project.pdf: project.tex
	xelatex project.tex

project.tex: project.md template.tex
	pandoc -f markdown+autolink_bare_uris --pdf-engine=xelatex project.md -o project.tex --template template.tex

project_short.pdf: project_short.tex
	xelatex project_short.tex

project_short.tex: project_short.md template.tex
	pandoc -f markdown+autolink_bare_uris --pdf-engine=xelatex project_short.md -o project_short.tex --template template.tex

clean:
	rm -fr project.aux project.log project.toc project.bbl project.bcf \
	       project.blg project.out project.run.xml project.tex *~
	rm -fr project_short.aux project_short.log project_short.toc project_short.bbl project_short.bcf \
	       project_short.blg project_short.out project_short.run.xml project_short.tex *~
