# Introduction

Some information about email encryption, including links to download
email clients and encryption software and for further information.

## Installation

To install the dependencies and create the PDF leaflet on a GNU/Linux
system:

    apt-get install pandoc texlive-full
    git clone <git_address.git> infosec_leaflet_encrypted_communications
    cd infosec_leaflet_email_encryption
    make

The leaflet will be named `project.pdf`.
