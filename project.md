---
title: Encryption Communications
subtitle:
version: 1.0
date: 16 October 2017
location: Juju's Bar And Stage, London
author: Fabio Natali
---

# Encrypted Communications

London, 16 October 2017

## Introduction

This section provides some background on cryptography and the concept
of public-key encryption. It can be skipped. :)

### A basic communication model

The following model can help understand what happens when two parties
engage in a conversation, be it in plain-text or encrypted.

![**A basic communication model** / Alice (**A**) sends a message to Bob (**B**), in plain-text (left) and then encrypted (right).]("img/communication_model"){width=600px, height=126px}

### Cryptography

In a strict definition, cryptography is about securing the
communication between two parties, so that a third party (an
"attacker" or "eavesdropper") cannot access the content of the
communication itself. More generally, modern cryptography has a wide
array of applications that go beyond the definition above to include,
for instance, authentication, digital signatures, and virtual
currencies.

### Public-key vs symmetric encryption

Traditionally, encryption involves the use of a secret key that has
been previously shared between two parties, eg Alice and Bob. Alice
and Bob can use this key to encrypt their messages and communicate
privately. This is known as **symmetric encryption**. Downsides: keys
must be shared beforehand through a secure channel; the number of keys
grows quadratically with the number of participants.

A new form of encryption was invented in the 70s that goes by the name
of **public-key cryptography**. With public-key cryptography every
user owns a pair of keys, one of which is secret and the other one is
public. If Bob wants to send a private message to Alice, he gets
Alice's public key - this does not require a private channel, as the
key is public anyway. Alice will be able to decrypt Bob's message
using her private key. There is no need to set different keys for
communicating with different people.

![**Symmetric vs Public-Key Encryption** / Alice (**A**) and Bob (**B**) want to encrypt their communications.]("img/symmetric_vs_public_key_encryption"){width=600px, height=166px}

## Messaging applications

A text messaging application is said to provide **end-to-end
encryption** when the content of the communication is encrypted from
the sender to the receiver. If properly implemented, end-to-end
encryption protects the communication against any third party,
including the company that created the application and the one that is
offering the service.

Nowadays, end-to-end encryption is used by a small number of
applications, such as Signal and WhatsApp - both relying on a
cryptographic protocol called the *Signal Protocol*.

We generally recommend **Signal** over WhatsApp since:

- Signal is **open source** while WhatsApp is **proprietary software**
  (ask us what this means)
- WhatsApp belongs to Facebook; there might be concerns about Facebook
  being able to collect communication **metadata** and combine them
  with the vast amount of information already in their hands

Signal is available for Android and iOS. It can also be installed on a
computer as an extension for the Chrome browser. Hands-on session:

- Install Signal
- Add a contact
- Start a chat, make a video call, start a group chat
- How to verify your contacts - how and why
- Disappearing messages
- What to do if you do not want to reveal your phone number

More information and references:

- Project web page https://signal.org/
- *How to: Use Signal for iOS* by Electronic Frontier Foundation
  https://ssd.eff.org/en/module/how-use-signal-ios
- *How to: Use Signal for Android* by Electronic Frontier Foundation
  https://ssd.eff.org/en/module/how-use-signal-android
- *Introduction to Signal Private Messenger* by Infosec Bytes
  https://www.youtube.com/watch?v=46ozjP-R2-E

## Email encryption on your computer

Public-key cryptography can be used to protect email
communications. There are a number of different applications and email
clients that support encryption, across all major operating
systems. We recommend the encryption software **GnuPG**, the email
client **Thunderbird** and a plugin called **Enigmail**.

A few important remarks: *free and open-source* vs *proprietary*
software; only download software from official websites and only via
*https*; what is a software *signature* and how to verify it.

Also important:

- Encryption vs *metadata*.
- The email subject does **not** get encrypted!! (This is fixed in
  recent versions of Enigmail.)
- Emails are encrypted while in transit, but the endpoints (your
  computer and the recipient's computer) are still vulnerable to
  attack, aka *black-bag cryptanalysis*.
- Some fine-tuning is necessary after GnuPG, Thunderbird, and Enigmail
  have been installed; eg: disable HTML email, set automatic
  encryption when replying to encrypted emails, ... See the
  *Configuration* section below and please refer to the *Security
  Self-Defense* and the *Security In A Box* online resources.

### The 10k-foot view

- (Optional) Create a dedicated email account (eg
  https://www.systemli.org/, https://riseup.net/,
  https://www.autistici.org/, ...)
- Download and install GnuPG (GNU/Linux: https://gnupg.org/; Mac OS X:
  https://gpgtools.org/; Windows: https://gpg4win.org/)
- Download and install Thunderbird
  (https://www.mozilla.org/en-US/thunderbird/)
- Install Enigmail (https://www.enigmail.net/)

### Configuration

- Enable `Encrypt/sign replies to encrypted/signed messages` and
  `Encrypt draft messages on saving`
- Disable `Send crash reports to the server` and HTML email (see
  https://securityinabox.org/en/guide/thunderbird/windows/)

### Exchanging keys

- Getting a public key from a URL; exporting your key as a file;
  publishing to or importing from a keyserver
- Fingerprints, man-in-the-middle attacks, the web-of-trust
- Revoking a key, what to do if a key gets compromised

## Online tutorials

The *Electronic Frontier Foundation* provides email encryption
tutorials as part of their *Security Self-Defense* project. Similarly,
the *Tactical Technology Collective* provides tutorials in their
*Security In A Box* project. Both tutorials have dedicated versions
for different operating systems.

- GNU/Linux: https://ssd.eff.org/en/module/how-use-pgp-linux and
  https://securityinabox.org/en/guide/thunderbird/linux/
- Mac OS X: https://ssd.eff.org/en/module/how-use-pgp-mac-os-x and
  https://securityinabox.org/en/guide/thunderbird/mac/
- Windows: https://ssd.eff.org/en/module/how-use-pgp-windows and
  https://securityinabox.org/en/guide/thunderbird/windows/

## Other resources and contacts

- The CryptoParty website: https://www.cryptoparty.in/
- EFF's *Surveillance Self-Defence*: https://ssd.eff.org
- Tactical Technology Collective's *Security in a box*:
  https://securityinabox.org/
- Access Now's *A First Look at Digital Security*:
  https://www.accessnow.org/a-first-look-at-digital-security/
- https://twitter.com/cryptopartyldn
- https://fabionatali.com / me@fabionatali.com (`6E0A 4E90 3241 34F0
  2788 445D 044A 25F5 267D C236`)
